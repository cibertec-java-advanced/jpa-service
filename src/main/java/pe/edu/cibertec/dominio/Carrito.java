package pe.edu.cibertec.dominio;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import pe.edu.cibertec.dominio.base.EntidadBase;

@Entity
public class Carrito extends EntidadBase {

    private Integer cantidad;
    private BigDecimal total;
    
    @Column(name="fecha_compra")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCompra;
    
    @ManyToOne
    @JoinColumn(name = "cliente_id")
    private Cliente cliente;
    
    /*
    @OneToMany sirve para mapear un objeto de tipo Carrito a muchos objetos
    de tipo DetalleCarrito. Puesto que será una relación bidireccional,
    es decir, que DetalleCarrito tiene un mapeo hacia Carrito, se DEBE indicar
    el campo `mappedBy`, que es el nombre del ATRIBUTO en la CLASE DetalleCarrito
    para identificar al objeto de tipo Carrito.
    */
    @OneToMany(mappedBy = "carritoCompras", fetch = FetchType.EAGER)
    private List<DetalleCarrito> detalleCarritoList;

    public Integer getCantidad() {
        return cantidad;
    }
    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }    
    public BigDecimal getTotal() {
        return total;
    }
    public void setTotal(BigDecimal total) {
        this.total = total;
    }
    public Date getFechaCompra() {
        return fechaCompra;
    }
    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }
    public Cliente getCliente() {
        return cliente;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    public List<DetalleCarrito> getDetalleCarritoList() {
        return detalleCarritoList;
    }
    public void setDetalleCarritoList(List<DetalleCarrito> detalleCarritoList) {
        this.detalleCarritoList = detalleCarritoList;
    }   
}
