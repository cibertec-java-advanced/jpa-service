package pe.edu.cibertec.dominio;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Table;
import pe.edu.cibertec.dominio.base.EntidadBase;

@Entity
@Table(name = "usuario")
public class Usuario extends EntidadBase {
    
    @Basic
    private String usuario;
    private String clave;

    public String getUsuario() {
        return usuario;
    }
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public String getClave() {
        return clave;
    }
    public void setClave(String clave) {
        this.clave = clave;
    }
}
