package pe.edu.cibertec.dominio;

import javax.persistence.Entity;
import javax.persistence.Table;
import pe.edu.cibertec.dominio.base.EntidadBase;

@Entity
@Table(name = "categoria")
public class Categoria extends EntidadBase {

    private String nombre;

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
