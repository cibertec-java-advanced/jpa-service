package pe.edu.cibertec.dominio.base;

import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class EntidadBase implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
    
    /*
    Ejemplo, en caso que todas mis tablas tengan campos de auditoría
    protected Date fechaCreacion;
    protected Date fechaUltimaActualizacion;
    protected String usuarioCreacion;
    protected String usuarioUltimaActualizacion;
    */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
