package pe.edu.cibertec.main;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import pe.edu.cibertec.dominio.Producto;
import pe.edu.cibertec.repositorio.ProductoRepositorio;
import pe.edu.cibertec.repositorio.impl.ProductoJpaRepositorioImpl;

public class Principal {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory( "labjpa" );
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        ProductoRepositorio pr = new ProductoJpaRepositorioImpl().setEntityManager(em);
        //List<Producto> pl = pr.obtenerPorCategoriaMarca(2L, 4L);
        List<Producto> pl = pr.obtenerTodos();
        pl.forEach(producto -> System.out.println(
                producto.getNombre() + " - " + 
                producto.getCategoria().getNombre() + " - " + 
                producto.getMarca().getNombre()
            )
        );

        em.getTransaction().commit();
        em.close();
        emf.close();
    }
}
