package pe.edu.cibertec.repositorio.impl;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import pe.edu.cibertec.dominio.Usuario;
import pe.edu.cibertec.repositorio.UsuarioRepositorio;

public class UsuarioJpaRepositorioImpl
    implements UsuarioRepositorio {

    private EntityManager em;

    public UsuarioJpaRepositorioImpl setEm(EntityManager em) {
        this.em = em;
        return this;
    }

    @Override
    public Usuario buscar(Long id) {
        return em.find(Usuario.class, id);
    }

    @Override
    public void crear(Usuario usuario) {
        em.persist(usuario);
    }

    @Override
    public void actualizar(Usuario usuario) {
        em.merge(usuario);
    }

    @Override
    public void eliminar(Usuario usuario) {
        em.remove(usuario);
    }

    @Override
    public Usuario iniciarSesion(String usuario, String clave) {
        try {
            String sql = "SELECT u FROM Usuario u WHERE u.usuario = :usuario AND u.clave = :clave";
            TypedQuery<Usuario> query = em.createQuery(sql, Usuario.class);
            query.setParameter("usuario", usuario);
            query.setParameter("clave", clave);
            return query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
}
