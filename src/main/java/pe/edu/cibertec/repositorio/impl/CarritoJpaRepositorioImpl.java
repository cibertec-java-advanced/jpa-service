package pe.edu.cibertec.repositorio.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import pe.edu.cibertec.dominio.Carrito;
import pe.edu.cibertec.dominio.DetalleCarrito;
import pe.edu.cibertec.repositorio.CarritoRepositorio;

public class CarritoJpaRepositorioImpl implements CarritoRepositorio {
    
    private EntityManager em;
    
    public CarritoRepositorio setEntityManager(EntityManager em) {
        this.em = em;
        return this;
    }

    @Override
    public List<Carrito> buscarPorUsuario(Long idUsuario) {
        String sql = "SELECT c FROM Carrito c WHERE c.usuario.id = :idUsuario"
                + " ORDER BY c.fechaCompra DESC";
        TypedQuery<Carrito> query = em.createQuery(sql, Carrito.class)
                .setParameter("idUsuario", idUsuario);
        return query.getResultList();
    }

    @Override
    public void crearCarrito(Carrito carrito, List<DetalleCarrito> detalleCarrito) {
        try {
            em.persist(carrito);
            detalleCarrito.forEach(data -> {
                data.setCarritoCompras(carrito);
                em.persist(data);
            });
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
