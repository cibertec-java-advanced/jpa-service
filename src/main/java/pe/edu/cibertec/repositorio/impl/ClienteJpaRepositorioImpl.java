package pe.edu.cibertec.repositorio.impl;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import pe.edu.cibertec.dominio.Cliente;
import pe.edu.cibertec.repositorio.ClienteRepositorio;

public class ClienteJpaRepositorioImpl
    implements ClienteRepositorio {

    private EntityManager em;

    public ClienteJpaRepositorioImpl setEm(EntityManager em) {
        this.em = em;
        return this;
    }

    @Override
    public Cliente buscarPorUsuario(Long usuarioId) {
        String sql = "SELECT c FROM Cliente c WHERE c.usuario.id = :usuarioId";
        TypedQuery<Cliente> query = em.createQuery(sql, Cliente.class);
        query.setParameter("usuarioId", usuarioId);
        return query.getSingleResult();
    }
}
