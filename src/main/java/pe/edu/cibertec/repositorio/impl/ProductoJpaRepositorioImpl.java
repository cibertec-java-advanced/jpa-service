package pe.edu.cibertec.repositorio.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import pe.edu.cibertec.dominio.Producto;
import pe.edu.cibertec.repositorio.ProductoRepositorio;

public class ProductoJpaRepositorioImpl implements ProductoRepositorio {

    private EntityManager em;

    public ProductoRepositorio setEntityManager(EntityManager em) {
        this.em = em;
        return this;
    }

    @Override
    public Producto buscar(Long id) {
        return em.find(Producto.class, id);
    }

    private static final String SELECT_PRODUCTOS =
        "SELECT p FROM Producto p INNER JOIN p.categoria c INNER JOIN p.marca m";

    @Override
    public List<Producto> obtenerTodos() {
        TypedQuery<Producto> query = em.createQuery(SELECT_PRODUCTOS, Producto.class);
        return query.getResultList();
    }

    @Override
    public List<Producto> obtenerPorCategoriaMarca(Long categoriaId, Long marcaId) {
        TypedQuery<Producto> query = em.createNamedQuery(
                Producto.NQ_OBTENER_PRODUCTO_POR_CATEGORIA_MARCA, Producto.class);
        query.setParameter("categoriaId", categoriaId);
        query.setParameter("marcaId", marcaId);
        return query.getResultList();
    }

    @Override
    public List<Producto> obtenerPorCategoriaCriteriaApi(Long idCategoria) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Producto> cq = cb.createQuery(Producto.class);
        Root<Producto> producto = cq.from(Producto.class);
        cq.select(producto).where(
                cb.equal(producto.get("categoria").get("id"), idCategoria)
        ).orderBy(
                cb.asc(producto.get("nombre"))
        );
        TypedQuery<Producto> query = em.createQuery(cq);
        return query.getResultList();
    }
}
