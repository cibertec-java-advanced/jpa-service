package pe.edu.cibertec.repositorio;

import java.util.List;
import pe.edu.cibertec.dominio.Producto;

public interface ProductoRepositorio {

    Producto buscar(Long id);

    List<Producto> obtenerTodos();

    List<Producto> obtenerPorCategoriaMarca(Long categoriaId, Long marcaId);

    List<Producto> obtenerPorCategoriaCriteriaApi(Long idCategoria);
}
