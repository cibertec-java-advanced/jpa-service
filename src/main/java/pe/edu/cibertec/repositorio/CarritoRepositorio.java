package pe.edu.cibertec.repositorio;

import java.util.List;
import pe.edu.cibertec.dominio.Carrito;
import pe.edu.cibertec.dominio.DetalleCarrito;

public interface CarritoRepositorio {

    List<Carrito> buscarPorUsuario(Long idUsuario);
    void crearCarrito(Carrito carrito, List<DetalleCarrito> detalleCarrito);
}
